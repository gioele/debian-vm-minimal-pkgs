NOW=$(shell date +%Y%m%d.%H%M.%s)
WITNESSES=$(patsubst %,.%,$(wildcard *.equivs))

all: $(WITNESSES)

.%.equivs: %.__equivs
	equivs-build --full "$<"
	touch "$@"

%.__equivs: %.equivs
	sed "$<" -e 's/^Version.*$$/Version: 1.0~dev$(NOW)/' -e 's/^ #INCLUDE:\(.*\)$$/ cat \1|sed -e "s_^_ _"/e' > "$@"

clean:
	rm -f *_1.0* $(WITNESSES)

upload:
	mv *_1.0* $(REPO_DIR)/repo-incoming

.PHONY: all update-versions clean upload
