debian-vm-minimal-pkgs
======================

Assorted package definitions needed to reduce the standard set of packages
required for the installation of a Debian system.

**ALL THESE PACKAGES ARE EXPERIMENTAL**

The root package `debian-vm-minimal` explicitly _Depends_ on all the
packages that are needed to bootstap a working Debian system.

## How to build

Install `equivs` and run `make`.

The Makefile will take care of adding timestamps to the version numbers,
thus each execution of `make` will create new packages that will
supersede the old ones.

```
$ sudo apt install equivs
$ make
```

## Changes required in other packages:

* `kmod`: **Fixed** ~~The `postinst` script embeds a Perl-script (luckily no longer
   needed) (Bug [#1009349](https://bugs.debian.org/1009349),
   MR: <https://salsa.debian.org/md/kmod/-/merge_requests/5>)~~

## Upstreaming of changes:

* `debian-vm-minimal`: Not started yet.

<!-- -->

* `adduser-sysusers`: Not started yet.
* `debconf`:
  - Mailing-list thread: <https://lists.debian.org/debian-boot/2022/08/msg00135.html>
  - To be tested with `simple-cdd` and `debian-cd`
  - [debconf] Moving `confmodule` to `debconf-common`: <https://salsa.debian.org/pkg-debconf/debconf/-/merge_requests/11>
  - [debconf] Auto-detection of cdebconf in `confmodule`: <https://salsa.debian.org/pkg-debconf/debconf/-/merge_requests/11>
  - [cdebconf] Dependency on `debconf-common`: <https://salsa.debian.org/installer-team/cdebconf/-/merge_requests/17>
  - [cdebconf] Complete replacement of `debconf`: WIP: <https://salsa.debian.org/gioele/cdebconf/-/commits/independence>
* `init-system-helpers`: Not started yet.
* `libpam-config-common`: Not started yet.
* `linux-base`:
  - Conversion into shell scripts: <https://salsa.debian.org/kernel-team/linux-base/-/merge_requests/8>
  - Alternative package `linux-base-shell`: <https://salsa.debian.org/gioele/linux-base/-/commits/shell-conversion>
